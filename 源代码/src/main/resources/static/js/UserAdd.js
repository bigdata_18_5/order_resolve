var userCode = null;
var ucFlag = false;
var userName = null;
var unFlag = false;
var userPassword = null;
var upFlag = false;
var ruserPassword = null;
var rupFlag = false;
var phone = null;
var phoneFlag = false;
var birthday = null;
var birFlag = false;
var userRole = null;
var idPicPath = null;
var workPicPath = null;
var urFlag = false;
var addBtn = null;


$(function(){
    userCode = $("#userCode");
    userName = $("#userName");
    userPassword = $("#userPassword");
    ruserPassword = $("#ruserPassword");
    phone = $("#phone");
    birthday = $("#birthday");
    userRole = $("#userRole");
    idPicPath = $("idPicPath");
    workPicPath = $("workPicPath");
    addBtn = $("#add");
    backBtn = $("#back");
    //初始化的时候，要把所有的提示信息变为：* 以提示必填项，更灵活，不要写在页面上
    userCode.next().html("*");
    userName.next().html("*");
    userPassword.next().html("*");
    ruserPassword.next().html("*");
    phone.next().html("*");
    birthday.next().html("*");
    // userRole.next().html("*");

    $.ajax({
        type:"POST",//请求类型
        url:"/user/userAdd",//请求的url
        data:{method:"getrolelist"},//请求参数
        dataType:"json",//ajax接口（请求url）返回的数据类型
        success:function(data){//data：返回数据（json对象）
            if(data != null){
                userRole.html("");
                var options = "<option value=\"0\">请选择</option>";
                for(var i = 0; i < data.length; i++){
                    //alert(data[i].id);
                    //alert(data[i].roleName);
                    options += "<option value=\""+data[i].id+"\">"+data[i].roleName+"</option>";
                }
                userRole.html(options);
            }
        },
        error:function(data){//当访问时候，404，500 等非200的错误状态码
            /*validateTip(userRole.next(),{"color":"red"},imgNo+" 获取用户角色列表error",false);*/
            userRole.next().css({"color": "red"});
            userRole.next().html("* 获取用户权限列表出现错误");
        }
    });



    /*
     * 验证
     * 失焦\获焦
     * jquery的方法传递
     * ajax后台验证--userCode是否已存在
     */
    userCode.bind("blur",function(){
        //ajax后台验证--userCode是否已存在
        $.ajax({
            type:"POST",//请求类型
            url:"/user/userAdd",//请求的url
            data:{method:"ucexist",userCode:userCode.val()},//请求参数
            dataType:"json",//ajax接口（请求url）返回的数据类型
            success:function(data){//data：返回数据（json对象）
                if(data.userCode == "exist"){//账号已存在，错误提示
                    /*validateTip(userCode.next(),{"color":"red"},imgNo+ " 该用户账号已存在",false);*/
                    userCode.next().css({"color": "red"});
                    userCode.next().html("* 该用户账号已存在");
                }else if (data.userCode == "null"){
                    userCode.next().css({"color": "orange"});
                    userCode.next().html("* 用户账号不能为空");
                }else{//账号可用，正确提示
                    /*validateTip(userCode.next(),{"color":"green"},imgYes+" 该账号可以使用",true);*/
                    userCode.next().css({"color": "green"});
                    userCode.next().html("* √");
                    ucFlag = true;
                }
            },
            error:function(data){//当访问时候，404，500 等非200的错误状态码
                /*validateTip(userCode.next(),{"color":"red"},imgNo+" 您访问的页面不存在",false);*/
                userCode.next().css({"color": "red"});
                userCode.next().html("* 您输入的用户账号不正确");
            }
        });


    }).bind("focus",function(){
        //显示友情提示
        /*validateTip(userCode.next(),{"color":"#666666"},"* 用户编码是您登录系统的账号",false);*/
        userCode.next().css({"color": "#666666"});
        userCode.next().html("* 用户编码是您登录系统的账号");
    }).focus();

    userName.bind("focus",function(){
        /*validateTip(userName.next(),{"color":"#666666"},"* 用户名长度必须是大于1小于10的字符",false);*/
        userName.next().css({"color": "#666666"});
        userName.next().html("* 用户姓名长度必须是大于1小于10的字符");
    }).bind("blur",function(){
        if(userName.val() != null && userName.val().length > 1
            && userName.val().length < 10){
            /*validateTip(userName.next(),{"color":"green"},imgYes,true);*/
            userName.next().css({"color": "green"});
            userName.next().html("* √");
            unFlag = true;
        }else{
            /*validateTip(userName.next(),{"color":"red"},imgNo+" 用户姓名输入的不符合规范，请重新输入",false);*/
            userName.next().css({"color": "red"});
            userName.next().html("* 用户姓名输入的不符合规范，请重新输入");
        }

    });

    userPassword.bind("focus",function(){
        /*validateTip(userPassword.next(),{"color":"#666666"},"* 密码长度必须是大于6小于20",false);*/
        userPassword.next().css({"color": "#666666"});
        userPassword.next().html("* 密码长度必须大于6小于20");
    }).bind("blur",function(){
        if(userPassword.val() != null && userPassword.val().length >= 6
            && userPassword.val().length < 20 ){
            /*validateTip(userPassword.next(),{"color":"green"},imgYes,true);*/
            userPassword.next().css({"color": "green"});
            userPassword.next().html("* √");
            upFlag = true;
        }else{
            /*validateTip(userPassword.next(),{"color":"red"},imgNo + " 密码输入不符合规范，请重新输入",false);*/
            userPassword.next().css({"color": "red"});
            userPassword.next().html("* 密码输入不符合规范，请重新输入");
        }
    });

    //验证第二次密码是否输入正确
    ruserPassword.bind("focus",function(){
        /*validateTip(ruserPassword.next(),{"color":"#666666"},"* 请输入与上面一只的密码",false);*/
        ruserPassword.next().css({"color": "#666666"});
        ruserPassword.next().html("* 请再次输入密码");
    }).bind("blur",function(){
        if(ruserPassword.val() != null && ruserPassword.val().length >= 6
            && ruserPassword.val().length < 20 && userPassword.val() == ruserPassword.val()){
            /*validateTip(ruserPassword.next(),{"color":"green"},imgYes,true);*/
            ruserPassword.next().css({"color": "green"});
            ruserPassword.next().html("* √");
            rupFlag = true;
        }else{
            /*validateTip(ruserPassword.next(),{"color":"red"},imgNo + " 两次密码输入不一致，请重新输入",false);*/
            ruserPassword.next().css({"color": "red"});
            ruserPassword.next().html("* 两次密码输入不一致，请重新输入");
        }
    });


    birthday.bind("focus",function(){
        /*validateTip(birthday.next(),{"color":"#666666"},"* 点击输入框，选择日期",false);*/
        birthday.next().css({"color": "#666666"});
        birthday.next().html("* 点击输入框，选择日期");
    }).bind("blur",function(){
        if(birthday.val() != null && birthday.val() != ""){
            /*validateTip(birthday.next(),{"color":"green"},imgYes,true);*/
            birthday.next().css({"color": "green"});
            birthday.next().html("* √");
            birFlag = true;
        }else{
            /*validateTip(birthday.next(),{"color":"red"},imgNo + " 选择的日期不正确,请重新输入",false);*/
            birthday.next().css({"color": "red"});
            birthday.next().html("* 日期不正确,请重新选择");
        }
    });

    phone.bind("focus",function(){
        /*validateTip(phone.next(),{"color":"#666666"},"* 请输入手机号",false);*/
        phone.next().css({"color": "#666666"});
        phone.next().html("* 请输入手机号");
    }).bind("blur",function(){
        /*var patrn=/^(13[0-9]|15[0-9]|18[0-9])\d{8}$/;*/
        var patrn=/^(1[3-9][0-9])\d{8}$/;
        if(phone.val().match(patrn)){
            /*validateTip(phone.next(),{"color":"green"},imgYes,true);*/
            phone.next().css({"color": "green"});
            phone.next().html("* √");
            phoneFlag = true;
        }else{
            /*validateTip(phone.next(),{"color":"red"},imgNo + " 您输入的手机号格式不正确",false);*/
            phone.next().css({"color": "red"});
            phone.next().html("* 输入的手机号格式不正确");
        }
    });

    userRole.bind("focus",function(){
        /*validateTip(userRole.next(),{"color":"#666666"},"* 请选择用户角色",false);*/
        userRole.next().css({"color": "#666666"});
        userRole.next().html("* 请选择用户角色");
    }).bind("blur",function(){
        if(userRole.val() != null && userRole.val() > 0){
            /*validateTip(userRole.next(),{"color":"green"},imgYes,true);*/
            userRole.next().css({"color": "green"});
            userRole.next().html("* √");
            urFlag = true;
        }else{
            /*validateTip(userRole.next(),{"color":"red"},imgNo + " 请重新选择用户角色",false);*/
            userRole.next().css({"color": "red"});
            userRole.next().html("* 请重新选择用户角色");
        }
    });

    addBtn.bind("click",function(){
        if(ucFlag && unFlag && upFlag && rupFlag
            && birFlag && phoneFlag) {
            if(confirm("是否确认提交数据")){
                swal('添加成功！','','success').then(function () {
                    $("#userForm").submit();
                });
            }
        } else {
            swal("请填写正确信息！","","info");
        }
    });

});