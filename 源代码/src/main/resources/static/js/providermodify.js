var proName = null;
var proContact = null;
var proPhone = null;
var proPhoneFlag = false;
var saveBtn = null;

$(function(){
	proName = $("#proName");
	proContact = $("#proContact");
	proPhone = $("#proPhone");
	saveBtn = $("#save");
	
	//初始化的时候，要把所有的提示信息变为：* 以提示必填项，更灵活，不要写在页面上
	// proContact.next().html("*");
	proPhone.next().html("*");
	
	/*
	 * 验证
	 * 失焦\获焦
	 * jquery的方法传递
	 */
/*	proName.on("focus",function(){
		validateTip(proName.next(),{"color":"#666666"},"* 请输入联系人",false);
	}).on("blur",function(){
		if(proName.val() != null && proContact.val() != ""){
			validateTip(proName.next(),{"color":"green"},imgYes,true);
		}else{
			validateTip(proName.next(),{"color":"red"},imgNo+" 联系人不能为空，请重新输入",false);
		}

	});

	proContact.on("focus",function(){
		validateTip(proContact.next(),{"color":"#666666"},"* 请输入联系人",false);
	}).on("blur",function(){
		if(proContact.val() != null && proContact.val() != ""){
			validateTip(proContact.next(),{"color":"green"},imgYes,true);
		}else{
			validateTip(proContact.next(),{"color":"red"},imgNo+" 联系人不能为空，请重新输入",false);
		}
		
	});*/
	
	proPhone.on("focus",function(){
		validateTip(proPhone.next(),{"color":"#666666"},"* 请输入手机号",false);
	}).on("blur",function(){
		var patrn=/^(1[0-9][0-9]|15[0-9]|18[0-9])\d{8}$/;
		if(proPhone.val().match(patrn)){
			/*validateTip(proPhone.next(),{"color":"green"},imgYes,true);*/
			proPhone.next().css({"color": "green"});
			proPhone.next().html("* √");
			proPhoneFlag = true;
		}else{
			/*validateTip(proPhone.next(),{"color":"red"},imgNo + " 您输入的手机号格式不正确",false);*/
			proPhone.next().css({"color": "red"});
			proPhone.next().html("* 您输入的手机号格式不正确");
		}
	});
	
	saveBtn.on("click",function(){
		/*proContact.blur();
		proPhone.blur();*/
		if(proPhoneFlag){
			if(confirm("是否确认提交数据")){
				swal('修改成功！','','success').then(function () {
					$("#providerForm").submit();
				});
			}
		} else {
			swal("请填写正确信息！","","info");
		}
	});

});