var oldpassword = null;
var newpassword = null;
var rnewpassword = null;
var saveBtn = null;
var oldFlag = false;
var newFlag = false;
var rNewFlag = false;

$(function(){
	oldpassword = $("#oldpassword");
	newpassword = $("#newpassword");
	rnewpassword = $("#rnewpassword");
	saveBtn = $("#save");

	oldpassword.next().html("*");
	newpassword.next().html("*");
	rnewpassword.next().html("*");

	oldpassword.on("blur",function(){
		$.ajax({
			type:"POST",
			url:"/user/pwdUpdate",
			data:{method:"oldpwd",oldpassword:oldpassword.val()},
			dataType:"json",
			success:function(data){
				if(data.result == "true"){//旧密码正确
					/*validateTip(oldpassword.next(),{"color":"green"},imgYes,true);*/
					oldpassword.next().css({"color": "green"});
					oldpassword.next().html("* 原密码正确");
					oldFlag = true;
					/*swal('原密码正确', '', 'success');*/
				}else if(data.result == "false"){//旧密码输入不正确
					/*validateTip(oldpassword.next(),{"color":"red"},imgNo + " 原密码输入不正确",false);*/
					oldpassword.next().css({"color": "red"});
					oldpassword.next().html("* 原密码输入不正确");
					/*swal('原密码错误', '', 'error');*/
				}else if(data.result == "sessionerror"){//当前用户session过期，请重新登录
					/*validateTip(oldpassword.next(),{"color":"red"},imgNo + " 当前用户session过期，请重新登录",false);*/
					oldpassword.next().css({"color": "orange"});
					oldpassword.next().html("* 当前用户session过期，请重新登录");
					/*swal('session错误', '当前用户session过期，请重新登录', 'info');*/
				}else if(data.result == "error"){//旧密码输入为空
					/*validateTip(oldpassword.next(),{"color":"red"},imgNo + " 请输入旧密码",false);*/
					oldpassword.next().css({"color": "orange"});
					oldpassword.next().html("* 原密码不能为空");
					/*swal('旧密码输入为空', '请输入旧密码', 'error');*/
				}
			},
			error:function(data){
				//请求出错
				/*validateTip(oldpassword.next(),{"color":"red"},imgNo + " 请求错误",false);*/
				oldpassword.next().css({"color": "red"});
				oldpassword.next().html("* 请求错误");
			}
		});


	}).on("focus",function(){
		/*validateTip(oldpassword.next(),{"color":"#666666"},"* 请输入原密码",false);*/
		oldpassword.next().css({"color": "#666666"});
		oldpassword.next().html("* 请输入原密码");
	});

	newpassword.on("focus",function(){
		/*validateTip(newpassword.next(),{"color":"#666666"},"* 密码长度必须是大于6小于20",false);*/
		newpassword.next().css({"color": "#666666"});
		newpassword.next().html("* 密码长度必须是大于6小于20");
	}).on("blur",function(){
		if(newpassword.val() != null && newpassword.val().length >= 6
			&& newpassword.val().length < 20 ){
			/*validateTip(newpassword.next(),{"color":"green"},imgYes,true);*/
			newpassword.next().css({"color": "green"});
			newpassword.next().html("* 密码合法");
			newFlag = true;
			/*oldpassword.next.attr(true);*/
		} else if (newpassword.val() == null || newpassword.val() == ""){
			newpassword.next().css({"color": "orange"});
			newpassword.next().html("* 新密码输入不能为空");
		} else{
			/*validateTip(newpassword.next(),{"color":"red"},imgNo + " 密码输入不符合规范，请重新输入",false);*/
			newpassword.next().css({"color": "red"});
			newpassword.next().html("* 密码输入不符合规范，请重新输入");
		}
	});


	rnewpassword.on("focus",function(){
		/*validateTip(rnewpassword.next(),{"color":"#666666"},"* 请输入与上面一致的密码",false);*/
		rnewpassword.next().css({"color": "#666666"});
		rnewpassword.next().html("* 请输入与上面一致的密码");
	}).on("blur",function(){
		if(rnewpassword.val() != null && rnewpassword.val().length >= 6
			&& rnewpassword.val().length < 20 && newpassword.val() == rnewpassword.val()){
			/*validateTip(rnewpassword.next(),{"color":"green"},imgYes,true);*/
			rnewpassword.next().css({"color": "green"});
			rnewpassword.next().html("* 密码一致");
			rNewFlag = true;
			/*oldpassword.next.attr(true);*/
		}else if (rnewpassword.val() == null || rnewpassword.val() == ""){
			rnewpassword.next().css({"color": "orange"});
			rnewpassword.next().html("* 确认密码输入不能为空");
		} else{
			/*validateTip(rnewpassword.next(),{"color":"red"},imgNo + " 两次密码输入不一致，请重新输入",false);*/
			rnewpassword.next().css({"color": "red"});
			rnewpassword.next().html("* 两次密码输入不一致，请重新输入");
		}
	});


	saveBtn.on("click",function(){
		oldpassword.blur();
		newpassword.blur();
		rnewpassword.blur();
		if(oldFlag && newFlag && rNewFlag){
			swal({
				title: "确认要修改密码吗？",
				text: "是否已记住新密码！",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: "确定",
				cancelButtonText: "取消",   //修改cancel显示的
				showLoaderOnConfirm: true  //加载等待时间的显示页面
			}).then(function () {
				/*$("#userForm").submit();*/
				/*window.location.href = "/user/pwdUpdate?method=savepwd"*/
				$.ajax({
					type: 'POST',
					url: '/user/pwdUpdate',
					data: {
						method:"savepwd",
						newpassword: newpassword.val()
					},
					dataType: "json",
					async: false,
					success: function (data) {  // 后端发字典过来 前端不需要你手动转 会自动帮你转换成js自定义对象
						if (data.code == "success") {
							swal(
								'修改成功！',
								'密码修改成功，请重新登录',
								'success'
							).then(function () {
								window.location.href = "/login/toLogin"
							});
						} else if (data.code == "error") {
							swal('修改失败！', '请重新尝试', 'info').then(function () {
								window.location.href = "/user/toPwd";
							});
						}
					},
					error: function () {
						swal("后台出错", "请重新执行该操作", "error");
					}
				})
			}, function (dismiss) {
				if (dismiss === 'cancel') {
					swal(
						'已取消！',
						'密码未修改',
						'info'
					);
				}
			})
			/*if(confirm("确定是否要修改密码？")){
                $("#userForm").submit();
            }*/
		} else {
			swal(
				'请输入正确的数据',
				'',
				'info'
			);
		}
	});
});