package com.zjn.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Page {
    private int pageNo;
    private int pageSize;
    private int totalPage;
    private int totalCount;
    private List<User> users;

    public Page(int pageNo, int pageSize, int totalPage, int totalCount, List<User> users) {
        this.pageNo=pageNo;
        this.pageSize=pageSize;
        this.totalPage=totalPage;
        this.totalCount=totalCount;
        this.users=users;
    }
}
