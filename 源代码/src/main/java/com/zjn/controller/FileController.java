package com.zjn.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Map;
@RestController
public class FileController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileController.class) ;
    /**
     * 测试单个文件上传
     */
    @RequestMapping("/upload1")
    public String upload1 (HttpServletRequest request, @RequestParam("file") MultipartFile file){
        Map<String, String[]> paramMap = request.getParameterMap() ;
        if (!paramMap.isEmpty()){
            LOGGER.info("paramMap == >>{}",paramMap);
        }
        try{
            if (!file.isEmpty()){
                // 打印文件基础信息
                LOGGER.info("Name == >>{}",file.getName());
                LOGGER.info("OriginalFilename == >>{}",file.getOriginalFilename());
                LOGGER.info("ContentType == >>{}",file.getContentType());
                LOGGER.info("Size == >>{}",file.getSize());
                // 文件输出地址
                String filePath = "D:\\Big_data\\IntelliJ IDEA 2019.2.1\\intellij_workspace\\smbms-springboot\\src\\main\\resources\\uploadfiles" ;
                new File(filePath).mkdirs();
                File writeFile = new File(filePath, file.getOriginalFilename());
                file.transferTo(writeFile);
                System.out.println("添加图片");
            }
            return "/user/UserAdd" ;
        } catch (Exception e){
            e.printStackTrace();
            return "系统异常" ;
        }
    }
    /**
     * 测试多文件上传
     */
//    @RequestMapping("/upload2")
//    public String upload2 (HttpServletRequest request, @RequestParam("file") MultipartFile[] fileList){
//        Map<String, String[]> paramMap = request.getParameterMap() ;
//        if (!paramMap.isEmpty()){
//            LOGGER.info("paramMap == >>{}",paramMap);
//        }
//        try{
//            if (fileList.length > 0){
//                for (MultipartFile file:fileList){
//                    // 打印文件基础信息
//                    LOGGER.info("Name == >>{}",file.getName());
//                    LOGGER.info("OriginalFilename == >>{}",file.getOriginalFilename());
//                    LOGGER.info("ContentType == >>{}",file.getContentType());
//                    LOGGER.info("Size == >>{}",file.getSize());
//                    // 文件输出地址
//                    String filePath = "F:/boot-file/" ;
//                    new File(filePath).mkdirs();
//                    File writeFile = new File(filePath, file.getOriginalFilename());
//                    file.transferTo(writeFile);
//                }
//            }
//            return "/user/UserAdd" ;
//        } catch (Exception e){
//            return "fail" ;
//        }
//    }
}

