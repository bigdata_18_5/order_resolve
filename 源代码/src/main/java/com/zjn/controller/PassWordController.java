package com.zjn.controller;

import com.alibaba.fastjson.JSONArray;
import com.zjn.config.Md5Algo;
import com.zjn.pojo.User;
import com.zjn.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@Controller
public class PassWordController {
    @Autowired
    private UserService userService;

    @GetMapping("/user/toPwd")
    public ModelAndView toPwdUpdate() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("user/PassWord");
        return mav;
    }

    @PostMapping("/user/pwdUpdate")
    public ModelAndView pwdUpdate(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView();
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();
        //封装用户的登录数据，这里的token是全局变量，可以在任意位置取出
        User currentUser = (User) subject.getPrincipal();
        //获取前端信息
        String method = request.getParameter("method");
        String password = request.getParameter("newpassword");
        //封装json数据
        Map<String, String> resultMap = new HashMap<String, String>();
        if (method.equals("savepwd") && method != null) {
            if (currentUser != null && !StringUtils.isEmpty(password)) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("id", currentUser.getId());
                map.put("userPassword", Md5Algo.md5Algo(password,currentUser.getSalt()));
                map.put("userCode", null);
                int flag = userService.updatePwd(map);
                if (flag > 0) {
                    resultMap.put("code", "success");
                    request.getSession().invalidate();
                    /*mav.setViewName("user/PassWord");*/
                } else {
                    resultMap.put("code", "error");
                }
            } /*else {
                mav.addObject("message", "新密码有问题");
                mav.setViewName("user/PassWord");
            }*/
        }
        //ajax动态异步验证旧密码
        else if (method.equals("oldpwd") && method != null) {
            String oldPwd = request.getParameter("oldpassword");
            //使用一个map来存放要向js页面传入的参数
            if (currentUser == null) {
                resultMap.put("result", "sessionerror");
            } else if (StringUtils.isEmpty(oldPwd)) {
                resultMap.put("result", "error");
            } else {
//                System.out.println("oldPWD:"+oldPwd+" "+currentUser.getUserPassword()+" "+currentUser.getSalt());
                if (currentUser.getUserPassword().equals(Md5Algo.md5Algo(oldPwd,currentUser.getSalt()))) {
                    resultMap.put("result", "true");
                } else {
                    resultMap.put("result", "false");
                }
            }
        }
        try {
            //设置接收的是json参数
            response.setContentType("application/json");
            PrintWriter writer = response.getWriter();
            writer.write(JSONArray.toJSONString(resultMap));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mav;
    }
}
