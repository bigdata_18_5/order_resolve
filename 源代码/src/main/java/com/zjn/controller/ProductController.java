package com.zjn.controller;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import com.zjn.config.Md5Algo;
import com.zjn.pojo.Bill;
import com.zjn.pojo.Product;
import com.zjn.pojo.Provider;
import com.zjn.pojo.User;
import com.zjn.service.BillService;
import com.zjn.service.ProductService;
import com.zjn.service.ProviderService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProviderService providerService;

    @Autowired
    private BillService billService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class) ;

    /*实现订单列表页面*/
    @GetMapping("/list")
    public ModelAndView ProductList(@RequestParam(required = false, defaultValue = "1", value = "pageNum") Integer pageNum,
                                 @RequestParam(defaultValue = "8", value = "pageSize") Integer pageSize,
                                 HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        //为了程序的严谨性，判断非空：
        if (pageNum == null || pageNum <= 0) {
            pageNum = 1;   //设置默认当前页
        }
        if (pageSize == null) {
            pageSize = 5;    //设置默认每页显示的数据数
        }

        //从前端获取参数
        String productName = request.getParameter("queryPN");
        String providerId = request.getParameter("queryPI");
        String isPay = request.getParameter("queryIsPay");
        if (productName == "") {
            productName = null;
        }
        if (providerId == null) {
            providerId = "0";
        }
        if (isPay == null) {
            isPay = "0";
        }

        //将查询需要的参数存入map中
        Map<String, Object> map = new HashMap<>();
        map.put("productName", productName);
        map.put("providerId", Integer.parseInt(providerId));

        //1.引入分页插件,pageNum是第几页，pageSize是每页显示多少条,默认查询总数count
        PageHelper.startPage(pageNum, pageSize);

        //2.紧跟的查询就是一个分页查询-必须紧跟.后面的其他查询不会被分页，除非再次调用PageHelper.startPage
        try {
            List<Product> productList = productService.getProductList(map);
            //为每个对象查出对应的供应商名
            for (Product product : productList) {
                Provider provider = new Provider();
                provider.setId(product.getProviderId());
                String proName = providerService.queryProByProvider(provider).getProName();
                product.setProviderName(proName);
            }
            //3.使用PageInfo包装查询后的结果,5是连续显示的条数,结果list类型是Page<E>
            PageInfo<Product> pageInfo = new PageInfo<Product>(productList, pageSize);

            //4.使用modelAndView将数据带回前端
            mav.addObject("pageInfo", pageInfo);
        } finally {
            PageHelper.clearPage(); //清理 ThreadLocal 存储的分页参数,保证线程安全
        }
        //获取供应商列表并传入前端
        map.put("proCode", null);
        map.put("proName", null);
        List<Provider> proList = providerService.getProList(map);
        mav.addObject("proList", proList);

        //将查询表中的参数返回，以增加用户的体验性
        mav.addObject("queryPN", productName);
        mav.addObject("queryPI", Integer.parseInt(providerId));
        mav.addObject("queryIsPay", Integer.parseInt(isPay));

        mav.setViewName("product/ProductList");
        return mav;
    }

    /*跳转到商品添加界面*/
    @GetMapping("/productAdd")
    public String toProductAdd() {
        return "product/productAdd";
    }

    /*实现商品添加*/
    @PostMapping("/productAdd")
    public ModelAndView productAdd(Product product, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView();
        String method = request.getParameter("method");
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();
        //封装用户的登录数据，这里的token是全局变量，可以在任意位置取出
        User currentUser = (User) subject.getPrincipal();
        //当method为add时，代表全部输入合法，成功添加
        if (method.equals("add") && method != null) {
            product.setCreationDate(new Date());
            product.setCreatedBy(currentUser.getId());
            int flag = 0;
            flag = productService.productAdd(product);
            if (flag > 0) {
                mav.setViewName("redirect:/product/list");
            } else {
                mav.setViewName("product/productAdd");
            }
        }
        //ajax动态获取供应商栏参数
        else if (method.equals("getproviderlist") && method != null) {
            List<Provider> providerList = providerService.getProListNoLimit(new HashMap<String, Object>());
            try {
                //将roleList转换成json对象
                response.setContentType("application/json;charset=utf-8");
                PrintWriter writer = response.getWriter();
                writer.write(JSONArray.toJSONString(providerList));
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //ajax动态验证登录账号productName是否已经存在
        else if (method.equals("bcexist") && method != null) {
            //判断订单编码是否可用
            String productName = request.getParameter("productName");
            //用于存放对billCode判断对结果
            Map<String, String> resultMap = new HashMap<String, String>();
            //判断billCode是否为空
            if (StringUtils.isEmpty(productName) || productName == "") {
                resultMap.put("productName", "empty");
            } else {
                Product product1 = new Product();
                product1.setProductName(productName);
                product1 = productService.queryProductById(product1);
                if (null != product1) {
                    resultMap.put("productName", "exist");
                } else {
                    resultMap.put("productName", "notexist");
                }
            }
            try {
                //把resultMap转为json字符串以json的形式输出
                //配置上下文的输出类型
                response.setContentType("application/json");
                //从response对象中获取往外输出的writer对象
                PrintWriter outPrintWriter = response.getWriter();
                //把resultMap转为json字符串 输出
                outPrintWriter.write(JSONArray.toJSONString(resultMap));
                outPrintWriter.flush();//刷新
                outPrintWriter.close();//关闭流
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mav;
    }



    @PostMapping("/productSave")
    @ResponseBody
    public ModelAndView productSave(Product product, HttpServletRequest request, HttpServletResponse response,@RequestParam("files") MultipartFile[] fileList) {
        ModelAndView mav = new ModelAndView();
        String method = request.getParameter("method");
        System.out.println(method);
        //当method为add时，代表全部输入合法，成功添加
        String birthday = request.getParameter("birthday");

        try{
            if (fileList.length > 0){

                for (MultipartFile file:fileList){
                    // 打印文件基础信息
                    LOGGER.info("Name == >>{}",file.getName());
                    LOGGER.info("OriginalFilename == >>{}",file.getOriginalFilename());
                    LOGGER.info("ContentType == >>{}",file.getContentType());
                    LOGGER.info("Size == >>{}",file.getSize());
                    // 文件输出地址
                    String filePath = "D:\\Big_data\\IntelliJ IDEA 2019.2.1\\intellij_workspace\\smbms-springboot\\src\\main\\resources\\static\\productPic" ;
                    new File(filePath).mkdirs();
                    File writeFile = new File(filePath, file.getOriginalFilename());
                    file.transferTo(writeFile);
                    String path = "/productPic/" + file.getOriginalFilename();
                    product.setProductPic(path);
                }
            }
        } catch (Exception e){
        }
        product.setCreationDate(new Date());
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();
        //封装用户的登录数据，这里的token是全局变量，可以在任意位置取出
        User currentUser = (User) subject.getPrincipal();
        product.setCreationDate(new Date());
        product.setCreatedBy(currentUser.getId());

        int flag = 0;
        flag = productService.productAdd(product);
        if (flag > 0) {
            mav.setViewName("redirect:/product/list");
        } else {
            mav.setViewName("/product/ProductAdd");
        }
        return mav;
    }


    /*订单查看详情*/
    @RequestMapping("/productView/{id}")
    public ModelAndView ProductView(@PathVariable("id") int productId) {
        ModelAndView mav = new ModelAndView();
        Product product = new Product();
        //根据id查询出当前选中的供应商
        product.setId(productId);
        product = productService.queryProductById(product);
        Provider provider = new Provider();
        provider.setId(product.getProviderId());
        product.setProviderName((providerService.queryProByProvider(provider)).getProName());
        //将数据传入前端
        mav.addObject("product", product);
        mav.setViewName("product/ProductView");
        return mav;
    }

    /*跳转到供应商修改页面*/
    @GetMapping("/productUpdate/{id}")
    public ModelAndView toProductModify(@PathVariable("id") int productId) {
        ModelAndView mav = new ModelAndView();
        Product product = new Product();
        //根据id查询出当前选中的供应商
        product.setId(productId);
        product = productService.queryProductById(product);
        //将数据传入前端
        mav.addObject("product", product);
        mav.setViewName("product/ProductUpdate");
        return mav;
    }

    /*商品修改*/
    @RequestMapping("/productUpdate")
    public ModelAndView ProductModify(Product product, HttpServletRequest req) {
        ModelAndView mav = new ModelAndView();
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();
        //封装用户的登录数据，这里的token是全局变量，可以在任意位置取出
        User currentUser = (User) subject.getPrincipal();
        //因为code设置为readonly，所以要将其手动注入对象属性中
        product.setProductName(req.getParameter("productName"));
        //将修改者和修改时间注入对象属性中
        product.setModifyBy(currentUser.getId());
        product.setModifyDate(new Date());
        int flag = productService.modifyProductById(product);
        if (flag > 0) {
            mav.setViewName("redirect:/product/list");
        } else {
            mav.setViewName("redirect:/product/productUpdate/" + product.getId());
        }
        return mav;
    }

    /*删除商品*/
    @GetMapping("/productDelete")
    public ModelAndView productDelete(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView();
        String id = request.getParameter("code");
        Map<String, String> resultMap = new HashMap<String, String>();
        int flag = productService.deleteProductById(Integer.parseInt(id));
        if (flag > 0) {
//            mav.setViewName("redirect:/user/list");
            resultMap.put("code", "success");
        } else {
//            mav.setViewName("redirect:/user/list");
            resultMap.put("code", "error");
        }
        try {
            //把resultMap转为json字符串以json的形式输出
            //配置上下文的输出类型
            response.setContentType("application/json");
            //从response对象中获取往外输出的writer对象
            PrintWriter outPrintWriter = response.getWriter();
            //把resultMap转为json字符串 输出
            outPrintWriter.write(JSONArray.toJSONString(resultMap));
            outPrintWriter.flush();//刷新
            outPrintWriter.close();//关闭流
        } catch (IOException e) {
            e.printStackTrace();
        }
        mav.setViewName("redirect:/product/list");
        return mav;
    }

    /*跳转到账单添加界面*/
    @GetMapping("/productBuy/{id}")
    public ModelAndView toProductBuy(@PathVariable("id") int productId) {
        ModelAndView mav = new ModelAndView();
        Product product = new Product();
        //根据id查询出当前选中的供应商
        product.setId(productId);
        product = productService.queryProductById(product);
        //将数据传入前端
        mav.addObject("product", product);
        mav.setViewName("product/ProductBuy");
        return mav;
    }

    /*实现账单添加*/
    @PostMapping(value = "/productBuy", produces = "application/json; charset=utf-8")
    public ModelAndView productBuy(Product product, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView();
        String method = request.getParameter("method");
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();
        //封装用户的登录数据，这里的token是全局变量，可以在任意位置取出
        User currentUser = (User) subject.getPrincipal();
        product.setProductName(request.getParameter("productName"));
        if (method.equals("getproviderlist") && method != null) {
            List<Provider> providerList = providerService.getProListNoLimit(new HashMap<String, Object>());
            try {
                //将roleList转换成json对象
                response.setContentType("application/json;charset=utf-8");
                PrintWriter writer = response.getWriter();
                writer.write(JSONArray.toJSONString(providerList));
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //当method为add时，代表全部输入合法，成功添加
        else{
            Bill bill = new Bill();
            bill.setCreationDate(new Date());
            bill.setCreatedBy(currentUser.getId());
            bill.setIsPayment(0);
            bill.setProductName(product.getProductName());
            bill.setProductDesc(product.getProductDesc());
            String productCount = request.getParameter("productCount");
            bill.setProductUnit(product.getProductUnit());
            bill.setProductCount(product.getProductValue());
            bill.setProviderId(product.getProviderId());
            double count = Double.valueOf(productCount);
            BigDecimal num = BigDecimal.valueOf(count);
            bill.setTotalPrice(num.multiply(product.getProductValue()));
            bill.setBillCode(new Date().toString().substring(10));
            int flag = 0;
            flag = billService.billAdd(bill);
            if (flag > 0) {
                mav.setViewName("redirect:/product/list");
            } else {
                mav.setViewName("product/ProductBuy");
            }
        }
        return mav;
    }
}
