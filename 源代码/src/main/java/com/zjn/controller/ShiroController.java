package com.zjn.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ShiroController {

    /*未授权页面设置*/
    @RequestMapping("/403")
    public String toUnauthorized(){
        return "error/403";
    }
}
