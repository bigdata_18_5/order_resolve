package com.zjn.controller;


import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zjn.config.Md5Algo;
import com.zjn.pojo.Role;
import com.zjn.pojo.User;
import com.zjn.service.RoleService;
import com.zjn.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@ResponseBody
public class ComserviceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileController.class) ;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;




    @GetMapping("/comservice/list")
    public ModelAndView comList(@RequestParam(required = false, defaultValue = "1", value = "pageNum") Integer pageNum,
                                  @RequestParam(defaultValue = "8", value = "pageSize") Integer pageSize,
                                  HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        //为了程序的严谨性，判断非空：
        if (pageNum == null || pageNum <= 0) {
            pageNum = 1;   //设置默认当前页
        }
        if (pageSize == null) {
            pageSize = 5;    //设置默认每页显示的数据数
        }

        //从前端获取参数
        String userName = request.getParameter("queryName");
        String queryUserRole = request.getParameter("queryUserRole");
        String userSex = request.getParameter("querySex");
        if (userName == ""){
            userName = null;
        }
        if (queryUserRole == null){
            queryUserRole = "2";
        }
        if (userSex == ""){
            userSex = null;
        }
        if(userSex==null){
            userSex="0";
        }
        if(userSex.equals("男")){
            userSex="1";
        }else if (userSex.equals("女")){
            userSex="2";
        }
        //将查询需要的参数存入map中
        Map<String, Object> map = new HashMap<>();
        map.put("userName", userName);
        map.put("userRole", Integer.parseInt(queryUserRole));
        map.put("userSex", userSex);
        //1.引入分页插件,pageNum是第几页，pageSize是每页显示多少条,默认查询总数count
        PageHelper.startPage(pageNum, pageSize);

        //2.紧跟的查询就是一个分页查询-必须紧跟.后面的其他查询不会被分页，除非再次调用PageHelper.startPage
        try {
            List<User> userList = userService.getUserList(map);
            //为每个对象查出对应的权限名
            for (User user : userList) {
                Role role = new Role();
                role.setId(user.getUserRole());
                String roleName = roleService.getRoleByRole(role).getRoleName();
                user.setUserRoleName(roleName);
            }
            //3.使用PageInfo包装查询后的结果,5是连续显示的条数,结果list类型是Page<E>
            PageInfo<User> pageInfo = new PageInfo<User>(userList, pageSize);

            //4.使用modelAndView将数据带回前端
            mav.addObject("pageInfo", pageInfo);
        } finally {
            PageHelper.clearPage(); //清理 ThreadLocal 存储的分页参数,保证线程安全
        }
        //获取权限名并传入前端


        if(userSex.equals("1")){
            userSex="男";
        }else if(userSex.equals("2")){
            userSex="女";
        }else {
            userSex="";
        }
        List<Role> roleList = roleService.getRoleList();
        mav.addObject("roleList", roleList);
        //将查询表中的参数返回，以增加用户的体验性
        mav.addObject("queryName", userName);
        mav.addObject("queryUserRole", Integer.parseInt(queryUserRole));
        mav.addObject("querySex", userSex);


        mav.setViewName("comservice/ComserviceList");
        return mav;
    }

    /*跳转到员工添加*/
    @GetMapping("/comservice/userAdd")
    public ModelAndView toUserAdd() {
        ModelAndView mav = new ModelAndView();
        List<Role> roleList = roleService.getRoleList();
        mav.addObject("roleList", roleList);
        mav.setViewName("comservice/UserAdd");
        return mav;
    }


    @Value("${web.upload-path}")
    private String path;
    /*员工添加*/
    @PostMapping("/comservice/userSave")
    @ResponseBody
    public ModelAndView userSave(User user, HttpServletRequest request, HttpServletResponse response, @RequestParam("files") MultipartFile[] fileList) {
        ModelAndView mav = new ModelAndView();
        String method = request.getParameter("method");
        System.out.println(method);
        //当method为add时，代表全部输入合法，成功添加
        String birthday = request.getParameter("birthday");
        try {
            user.setBirthday(new SimpleDateFormat("yyyy-MM-dd").parse(birthday));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try{
            if (fileList.length > 0){
                int idx = 0;
                for (MultipartFile file:fileList){
                    // 打印文件基础信息
                    LOGGER.info("Name == >>{}",file.getName());
                    LOGGER.info("OriginalFilename == >>{}",file.getOriginalFilename());
                    LOGGER.info("ContentType == >>{}",file.getContentType());
                    LOGGER.info("Size == >>{}",file.getSize());
                    // 文件输出地址
                    String filePath = "D:\\Big_data\\IntelliJ IDEA 2019.2.1\\intellij_workspace\\smbms-springboot\\src\\main\\resources\\static\\uploadfiles" ;
                    new File(filePath).mkdirs();
                    File writeFile = new File(filePath, file.getOriginalFilename());
                    file.transferTo(writeFile);
                    String path = "/uploadfiles/" + file.getOriginalFilename();
                    if(idx==0)
                        user.setIdPicPath(path);
                    else
                        user.setWorkPicPath(path);
                    idx++;
                }
            }
        } catch (Exception e){
        }
        user.setCreationDate(new Date());
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();
        //封装用户的登录数据，这里的token是全局变量，可以在任意位置取出
        User currentUser = (User) subject.getPrincipal();
        user.setSalt(Md5Algo.randomSalt());
        user.setUserPassword(Md5Algo.md5Algo(user.getUserPassword(),user.getSalt()));
        user.setCreatedBy(currentUser.getId());

        int flag = 0;
        flag = userService.userAdd(user);
        if (flag > 0) {
            mav.setViewName("redirect:/comservice/list");
        } else {
            mav.setViewName("/comservice/UserAdd");
        }
        return mav;
    }


}
