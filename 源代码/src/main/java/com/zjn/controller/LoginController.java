package com.zjn.controller;

import com.zjn.config.UserRealm;
import com.zjn.pojo.User;
import com.zjn.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;
    @Autowired
    private DefaultSecurityManager securityManager;
    /*跳转到用户登录页面*/
    @RequestMapping("/toLogin")
    public String toLogin(){
        return "index";
    }

    /*用户登录*/
    //改进后将通过shiro来进行验证
    @PostMapping("/login")
    public ModelAndView login(User user, HttpServletRequest request){
        ModelAndView mav = new ModelAndView();
        //获取当前用户

        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken token = new UsernamePasswordToken(user.getUserCode(), user.getUserPassword());
        System.out.println(user.getUserCode()+" "+user.getUserPassword());


        //执行登录的方法
        try {
            subject.login(token);
            System.out.println("认证状态"+subject.isAuthenticated());
            user.setUserPassword(null);
            user = userService.queryUserByUser(user);
            request.getSession().setAttribute("username",user.getUserName());
            mav.addObject("name",user.getUserName());
            mav.setViewName("redirect:/main.html");
        } catch (UnknownAccountException e) {    //捕获用户名不存在的异常
            mav.addObject("msg","用户名错误");
            mav.setViewName("/index.html");
        } catch (IncorrectCredentialsException e) { //捕获密码错误的异常
            e.printStackTrace();
            mav.addObject("msg","密码错误");
            //如果只是密码错误，可以让页面仍然保留当前用户名
            mav.addObject("userCode",user.getUserCode());
            mav.setViewName("/index.html");
        }


        return mav;
    }

    /*用户注销：交给shiro完成，在shiroConfig中配置即可*/
    /*@GetMapping("/logout")
    public ModelAndView logout(HttpSession session){
        ModelAndView mav = new ModelAndView();
        session.invalidate();
        mav.addObject("msg","已注销，请重新登录");
        mav.setViewName("index");
        return mav;
    }*/
}
