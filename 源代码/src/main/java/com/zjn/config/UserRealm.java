package com.zjn.config;

import com.zjn.mapper.UserMapper;
import com.zjn.pojo.User;
import lombok.extern.log4j.Log4j;
import org.apache.juli.logging.Log;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;


//自动移的UserRealm
public class UserRealm extends AuthorizingRealm {

    @Autowired
    private UserMapper userMapper;



    private static final String OR_OPERATOR = " OR ";
    private static final String AND_OPERATOR = " AND ";
    @Override
    public boolean isPermitted(PrincipalCollection principals, String permission) {
        //return super.isPermitted(principals,permission);
        if (permission.contains(OR_OPERATOR)) {
            //如果有任何一个权限，返回true，否则返回false
            String[] permissions = permission.split(OR_OPERATOR);
            for (String p : permissions) {
                //只要有一个权限是通过验证的就返回true
                if (super.isPermitted(principals,p)) {
                    return true;
                }
            }
            return false;

        }else if (permission.contains(AND_OPERATOR)) {

            String[] permissions = permission.split(AND_OPERATOR);
            for (String p : permissions) {
                //只要有一个权限不是true的，我们就返回假
                if (!super.isPermitted(principals,p)) {
                    return false;
                }
            }
            return true;

        }else{//不包含 OR AND 的走原来逻辑
            return super.isPermitted(principals,permission);
        }
    }

    /*授权
    *   获取角色权限信息*/
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("执行了授权方法->doGetAuthorizationInfo");

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        /*为当前对象赋予perms权限*/
//        info.addStringPermission("emp:emp");
        //1. 拿到当前登录的对象
        Subject subject = SecurityUtils.getSubject();
        //2. 取出下面认证类最后存入SimpleAuthenticationInfo中的值：即为当前用户
        User currentUser = (User) subject.getPrincipal();
        //3. 设置当前用户权限
        if(currentUser.getUserRole()==1){
            currentUser.setPerms("user:admin");
        }else if(currentUser.getUserRole()==2){
            currentUser.setPerms("user:manager");
        }else if(currentUser.getUserRole()==3){
            currentUser.setPerms("user:worker");
        }else if(currentUser.getUserRole()==4){
            currentUser.setPerms("user:simple");
        }

        info.addStringPermission(currentUser.getPerms());
        return info;
    }

    /*认证
    *   获取用户凭证信息*/
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("执行了认证方法->doGetAuthenticationInfo");
//
//        //获取token
//        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
//        //连接真实的数据库
//        User user = new User();
//        user.setUserCode(token.getUsername());      //从token中获取用户名
//        user = userMapper.queryUserByUser(user);
//        //没有这个用户
//        if (user == null) {
//            return null;    //相当于UnknownAccountException异常
//        }
//        //密码认证，这里已经不显示密码验证而是由shiro自己来做
//        return new SimpleAuthenticationInfo(user, user.getUserPassword(), "");
//


        String userName = (String) authenticationToken.getPrincipal();//原始输入的用户名
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        User user = new User();
        user.setUserCode(token.getUsername());      //从token中获取用户名
        user = userMapper.queryUserByUser(user);//根据原始用户名查询得到的数据库中的用户信息

        if(userName.equals(user.getUserCode())){
            SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(user,
                    user.getUserPassword(),
                    ByteSource.Util.bytes(user.getSalt()),
                    getName());

            return  simpleAuthenticationInfo;

        }
        return null;
    }
}
