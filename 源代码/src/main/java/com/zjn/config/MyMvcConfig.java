package com.zjn.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    /*指定那些路径会跳转到index页面*/
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/index.html").setViewName("index");
        registry.addViewController("/main.html").setViewName("dashboard");
        registry.addViewController("/user.html").setViewName("user/UserAdd");
        registry.addViewController("/file.ftl").setViewName("user/test");
        registry.addViewController("/pwd.html").setViewName("user/PassWord");
        registry.addViewController("/toUnauthorizedByMe").setViewName("error/404");
    }
    @Value("${web.upload-path}")
    private String path;
    /*将自定义的国际化解析器注入到bean中*/
    @Bean
    public LocaleResolver localeResolver(){
        return new MyLocaleResolver();
    }
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/uploadfiles/**").addResourceLocations(path);
    }

}
