package com.zjn.config;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.junit.Test;

import java.util.Random;

public class Md5Algo {
    public static String md5Algo(String PWD, String Salt){
        Md5Hash md5Hash = new Md5Hash(PWD,Salt,1024);
        return md5Hash.toHex();
    }
    public static String randomSalt(){
        return String.format("%04d",new Random().nextInt(9999));
    }
    @Test
    public void test1(){
        Md5Hash md5Hash3 = new Md5Hash("0000000","1234",1024);
        String s2 = md5Hash3.toHex();
        System.out.println(s2);
    }

}
