package com.zjn.service.Impl;

import com.zjn.mapper.ProductMapper;
import com.zjn.pojo.Bill;
import com.zjn.pojo.Product;
import com.zjn.service.BillService;
import com.zjn.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    /*通过id查询特定的单个商品
     * 但参数仍然是Product对象，方便以后需要多条件查询时，方便修改
     * 通过动态sql还可以实现productName查询*/
    public Product queryProductById(Product product) {
        return productMapper.queryProductById(product);
    }

    /*
    获取商品列表,map包含(String billCode, String productName,int currentPageNo,int pageSize)
        1. 查询全部商品
        2. 通过编码查询
        3. 通过商品名称查询
        4. 通过编码+名称查询
    */
    public List<Product> getProductList(Map<String, Object> map) { return productMapper.getProductList(map); }

    /*商品添加*/
    public int productAdd(Product product) { return productMapper.productAdd(product); }


    /*通过id删除商品*/
    public int deleteProductById(int id) { return productMapper.deleteProductById(id); }

    /*通过id修改商品*/
    public int modifyProductById(Product product) { return productMapper.modifyProductById(product); }

    /*根据供应商id（providerId）获取订单数量*/
    public int getProductCountByProId(int providerId) {
        return productMapper.getProductCountByProId(providerId);
    }

}
