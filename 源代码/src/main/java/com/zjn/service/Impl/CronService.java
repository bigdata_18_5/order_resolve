package com.zjn.service.Impl;

import com.zjn.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CronService {

    @Autowired
    MailService mailService;

    @Scheduled(cron = "0 0/10 * * * ?")
    public void send(){
        mailService.sendSimpleMail();
        System.out.println(new Date());
    }
}
