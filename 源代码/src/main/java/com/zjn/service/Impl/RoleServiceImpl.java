package com.zjn.service.Impl;

import com.zjn.mapper.RoleMapper;
import com.zjn.pojo.Role;
import com.zjn.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    /*获取角色列表*/
    public List<Role> getRoleList() {
        return roleMapper.getRoleList();
    }

    /*通过id获取角色*/
    public Role getRoleByRole(Role role) {
        return roleMapper.getRoleByRole(role);
    }

    @Override
    public int addRole(Role role) {
        return roleMapper.addRole(role);
    }

    @Override
    public int deleteRoleById(int id) {
        return roleMapper.deleteRoleById(id);
    }

}
