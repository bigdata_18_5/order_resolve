package com.zjn.service;

import com.zjn.pojo.Product;

import java.util.List;
import java.util.Map;

public interface ProductService {
    /*通过id查询特定的单个商品
     * 但参数仍然是Product对象，方便以后需要多条件查询时，方便修改
     * 通过动态sql还可以实现productName查询*/
    Product queryProductById(Product product);

    /*
    获取订单列表,map包含(String billCode, String productName,int currentPageNo,int pageSize)
        1. 查询全部订单
        2. 通过编码查询
        3. 通过商品名称查询
        4. 通过编码+名称查询
    */
    List<Product> getProductList(Map<String, Object> map);

    /*订单添加*/
    int productAdd(Product product);

    /*通过id删除订单*/
    int deleteProductById(int id);

    /*通过id修改订单*/
    int modifyProductById(Product product);

    /*根据供应商id（providerId）获取商品数量*/
    int getProductCountByProId(int providerId);
}
