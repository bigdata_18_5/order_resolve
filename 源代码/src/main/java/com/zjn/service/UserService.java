package com.zjn.service;

import com.zjn.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    /*用户登录验证*/
    User queryUserByUser(User user);

    /*修改用户密码map包含（int id,String password）*/
    int updatePwd(Map<String,Object> map);

    /*根据用户名或角色获取用户数量map包含（String userName,int userRole）*/
    int queryUserCount(Map<String,Object> map);

    /*
    分页专用：用户列表,map包含(String userName,int userRole,int count,int pageSize)
        1. 查询全部用户
        2. 通过姓名查询
        3. 通过角色查询
        4. 通过姓名+角色查询
    */
    List<User> pageUserList(Map<String,Object> map);

    /*
    获取用户列表（并在mapper的基础上增加分页实现）
    map包含(String userName,int userRole,int currentPageNo,int pageSize)
        1. 查询全部用户
        2. 通过姓名查询
        3. 通过角色查询
        4. 通过姓名+角色查询
    */
    List<User> getUserList(Map<String,Object> map);

    /*用户添加*/
    int userAdd(User user);

    /*通过id删除用户*/
    int deleteUserById(int id);

    /*通过id修改用户*/
    int modifyUserById(User user);


}
