package com.zjn.service;

import com.zjn.pojo.Provider;

import java.util.List;
import java.util.Map;

public interface ProviderService {
    /*通过id查询特定的单个供应商
    * 但参数仍然是provider对象，方便以后需要多条件查询时，方便修改
    * 通过动态sql还可以实现proCode和proName但查询
    */
    Provider queryProByProvider(Provider provider);

    /*
    获取供应商列表(并在mapper的基础上增加分页实现）
    map包含(String proCode, String proName,int currentPageNo,int pageSize)
        1. 查询全部供应商
        2. 通过编码查询
        3. 通过名称查询
        4. 通过编码+名称查询
    */
    List<Provider> getProList(Map<String,Object> map);

    /*不包含分页的获取供应商列表,map包含(String proCode, String proName)
        1. 查询全部供应商
        2. 通过编码查询
        3. 通过名称查询
        4. 通过编码+名称查询
    */
    List<Provider> getProListNoLimit(Map<String,Object> map);

    /*供应商添加*/
    int proAdd(Provider provider);

    /*通过id删除供应商*/
    int deleteProById(int id);

    /*通过id修改供应商*/
    int modifyProById(Provider provider);

    /*通过编码proCode或姓名查询用户（没有模糊查询）*/
    Provider queryProNoLike(Provider provider);

    /*根据供应商编码或名称获取用户数量,map包含（String proCode, String proName）*/
    int queryProCount(Map<String,Object> map);
}
