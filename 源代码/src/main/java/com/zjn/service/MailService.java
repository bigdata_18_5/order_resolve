package com.zjn.service;

import javax.mail.MessagingException;

public interface MailService {
    void sendSimpleMail();

    void sendInlineResourceMail(String to, String subject, String content, String rscPath, String rscId) throws MessagingException;

}
