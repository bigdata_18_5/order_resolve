package com.zjn.mapper;

import com.zjn.pojo.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface RoleMapper {
    /*获取角色列表*/
    List<Role> getRoleList();

    /*通过权限对象获取角色*/
    Role getRoleByRole(Role role);

    /*添加职位*/
    int addRole(Role role);

    /*删除职位*/
    int deleteRoleById(@Param("id") int id);


}
