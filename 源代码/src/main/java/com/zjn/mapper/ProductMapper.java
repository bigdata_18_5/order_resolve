package com.zjn.mapper;

import com.zjn.pojo.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ProductMapper {
    /*通过id查询特定的单个商品
     * 但参数仍然是Product对象，方便以后需要多条件查询时，方便修改
     * 通过动态sql还可以实现productName查询*/
    Product queryProductById(Product product);

    /*
    获取商品列表,map包含(String productName,int providerId,int isPayment,int currentPageNo,int pageSize)
        1. 查询全部商品
        2. 通过供应商查询
        3. 通过商品名称查询
        4. 通过是否支付查询
        4. 通过供应商+名称+是否支付查询
    */
    List<Product> getProductList(Map<String, Object> map);

    /*商品添加*/
    int productAdd(Product product);

    /*通过id删除商品*/
    int deleteProductById(@Param("id") int id);

    /*通过id修改订单*/
    int modifyProductById(Product product);

    /*根据供应商id（providerId）获取商品数量*/
    int getProductCountByProId(@Param("providerId") int providerId);
}
