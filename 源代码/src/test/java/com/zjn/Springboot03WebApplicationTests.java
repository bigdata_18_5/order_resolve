package com.zjn;

import com.zjn.pojo.Bill;
import com.zjn.service.BillService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class Springboot03WebApplicationTests {

    @Autowired
    private BillService billService;

    @Test
    void contextLoads() {
        Map<String, Object> map = new HashMap<String, Object>();
        String id = "14";
        map.put("providerId", Integer.parseInt(id));
        List<Bill> billList = billService.getBillList(map);
        System.out.println(Integer.parseInt(id));
        System.out.println(billList);
    }

}
