var proCode = null;
var pcFlag = false;
var proName = null;
var pnFlag = false;
var proContact = null;
var pctFlag = false;
var proPhone = null;
var ppFlag = false;
var addBtn = null;

$(function(){
	proCode = $("#proCode");
	proName = $("#proName");
	proContact = $("#proContact");
	proPhone = $("#proPhone");
	addBtn = $("#add");
	//初始化的时候，要把所有的提示信息变为：* 以提示必填项，更灵活，不要写在页面上
	proCode.next().html("*");
	proName.next().html("*");
	proContact.next().html("*");
	proPhone.next().html("*");
	
	/*
	 * 验证
	 * 失焦\获焦
	 * jquery的方法传递
	 */
	proCode.bind("blur",function(){
		//ajax后台验证--proCode是否已存在
		$.ajax({
			type:"POST",//请求类型
			url:"/pro/proAdd",//请求的url
			data:{method:"pcExist",proCode:proCode.val()},//请求参数
			dataType:"json",//ajax接口（请求url）返回的数据类型
			success:function(data){//data：返回数据（json对象）
				if(data.proCode == "exist"){//账号已存在，错误提示
					/*validateTip(proCode.next(),{"color":"red"},imgNo+ " 该编码已存在",false);*/
					proCode.next().css({"color": "red"});
					proCode.next().html("* 该编码已存在");
				}else if (data.proCode == "notExist"){//账号可用，正确提示
					/*validateTip(proCode.next(),{"color":"green"},imgYes+" 该编码可以使用",true);*/
					proCode.next().css({"color": "green"});
					proCode.next().html("* √");
					pcFlag = true;
				}else if (data.proCode == "empty"){
					/*validateTip(proCode.next(),{"color":"red"},imgNo+" 编码不能为空，请重新输入",false);*/
					proCode.next().css({"color": "orange"});
					proCode.next().html("* 编码不能为空，请重新输入");
				}
			},
			error:function(data){//当访问时候，404，500 等非200的错误状态码
				/*validateTip(proCode.next(),{"color":"red"},imgNo+" 您访问的页面不存在",false);*/
				proCode.next().css({"color": "red"});
				proCode.next().html("* 访问出错了...");
			}
		});
	}).bind("focus",function(){
		//显示友情提示
		/*validateTip(proCode.next(),{"color":"#666666"},"* 请输入供应商编码",false);*/
		proCode.next().css({"color": "#666666"});
		proCode.next().html("* 请输入供应商编码");
	}).focus();
	
	proName.bind("blur",function(){
		//ajax后台验证--proName是否已存在
		$.ajax({
			type:"POST",//请求类型
			url:"/pro/proAdd",//请求的url
			data:{method:"pnExist",proName:proName.val()},//请求参数
			dataType:"json",//ajax接口（请求url）返回的数据类型
			success:function(data){//data：返回数据（json对象）
				if(data.proName == "exist"){//名称已存在，错误提示
					/*validateTip(proName.next(),{"color":"red"},imgNo+ " 该供应商名称已存在",false);*/
					proName.next().css({"color": "red"});
					proName.next().html("* 该供应商名称已存在");
				}else if (data.proName == "notExist"){//名称可用，正确提示
					/*validateTip(proName.next(),{"color":"green"},imgYes+" 该名称可以使用",true);*/
					proName.next().css({"color": "green"});
					proName.next().html("* √");
					pnFlag = true;
				}else if (data.proName == "empty"){//名称不能为空
					/*validateTip(proName.next(),{"color":"red"},imgNo+" 供应商名称不能为空，请重新输入",false);*/
					proName.next().css({"color": "orange"});
					proName.next().html("* 供应商名称不能为空，请重新输入");
				}
			},
			error:function(data){//当访问时候，404，500 等非200的错误状态码
				/*validateTip(proName.next(),{"color":"red"},imgNo+" 您访问的页面不存在",false);*/
				proName.next().css({"color": "red"});
				proName.next().html("* 访问出错了...");
			}
		});
	}).bind("focus",function(){
		/*validateTip(proName.next(),{"color":"#666666"},"* 请输入供应商名称",false);*/
		proName.next().css({"color": "#666666"});
		proName.next().html("* 请输入供应商名称");
	}).focus();
	
	proContact.on("focus",function(){
		/*validateTip(proContact.next(),{"color":"#666666"},"* 请输入联系人",false);*/
		proContact.next().css({"color": "#666666"});
		proContact.next().html("* 请输入联系人姓名");
	}).on("blur",function(){
		if(proContact.val() != null && proContact.val() != ""){
			/*validateTip(proContact.next(),{"color":"green"},imgYes,true);*/
			proContact.next().css({"color": "green"});
			proContact.next().html("* √");
			pctFlag = true;
		}else{
			/*validateTip(proContact.next(),{"color":"red"},imgNo+" 联系人不能为空，请重新输入",false);*/
			proContact.next().css({"color": "orange"});
			proContact.next().html("* 联系人不能为空，请重新输入");
		}
		
	});
	
	proPhone.on("focus",function(){
		/*validateTip(proPhone.next(),{"color":"#666666"},"* 请输入手机号",false);*/
		proPhone.next().css({"color": "#666666"});
		proPhone.next().html("* 请输入手机号");
	}).on("blur",function(){
		var patrn=/^(1[3-9][0-9])\d{8}$/;
		if(proPhone.val().match(patrn)){
			/*validateTip(proPhone.next(),{"color":"green"},imgYes,true);*/
			proPhone.next().css({"color": "green"});
			proPhone.next().html("* √");
			ppFlag = true;
		}else{
			/*validateTip(proPhone.next(),{"color":"red"},imgNo + " 您输入的手机号格式不正确",false);*/
			proPhone.next().css({"color": "red"});
			proPhone.next().html("* 您输入的手机号格式不正确，请重新输入");
		}
	});
	
	addBtn.bind("click",function(){
		if(pcFlag && pnFlag && pctFlag && ppFlag){
			if(confirm("是否确认提交数据")){
				swal('添加成功！','','success').then(function () {
					$("#providerForm").submit();
				});
			}
		}else {
			swal("请填写正确信息！","","info");
		}
	});

});