var productName = null;
var pnFlag = false;
var productUnit = null;
var puFlag = false;
var productCount = null;
var pcFlag = false;
var totalPrice = null;
var tpFlag = false;
var providerId = null;
var useName = null;
var addBtn = null;

function priceReg (value){
	value = value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
		value = value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
    value = value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
    value = value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");//去掉特殊符号￥
	if(value.indexOf(".")>0){
		value = value.substring(0,value.indexOf(".")+3);
	}
	return value;
}


$(function(){
	productName = $("#productName");
	productUnit = $("#productUnit");
	productCount = $("#productCount");
	totalPrice = $("#totalPrice");
	providerId = $("#providerId");
	userName = $("#userName");
	addBtn = $("#save");
	
	//初始化的时候，要把所有的提示信息变为：* 以提示必填项，更灵活，不要写在页面上
	productName.next().html("*");
	productUnit.next().html("*");
	productCount.next().html("*");
	totalPrice.next().html("*");
	providerId.next().html("*");
	userName.next().html("*");

	$.ajax({
		type:"POST",//请求类型
		url:"/bill/billAdd",//请求的url
		data:{method:"getproviderlist"},//请求参数
		dataType:"json",//ajax接口（请求url）返回的数据类型
		success:function(data){//data：返回数据（json对象）
			if(data != null){
				var pid = $("#pid").val();
				$("select").html("");//通过标签选择器，得到select标签，适用于页面里只有一个select
				var options = "<option value=\"0\">请选择</option>";
				for(var i = 0; i < data.length; i++){
					//alert(data[i].id);
					//alert(data[i].proName);
					if(pid != null && pid != undefined && data[i].id == pid ){
						options += "<option selected=\"selected\" value=\""+data[i].id+"\" >"+data[i].proName+"</option>";
					}else{
						options += "<option value=\""+data[i].id+"\" >"+data[i].proName+"</option>";
					}
					
				}
				$("select").html(options);
			}
		},
		error:function(data){//当访问时候，404，500 等非200的错误状态码
			/*validateTip(providerId.next(),{"color":"red"},imgNo+" 获取供应商列表error",false);*/
			providerId.next().css({"color": "red"});
			providerId.next().html("* 获取供应商列表错误");
		}
	});
	/*
	 * 验证
	 * 失焦\获焦
	 * jquery的方法传递
	 */
	
	productName.on("focus",function(){
		/*validateTip(productName.next(),{"color":"#666666"},"* 请输入商品名称",false);*/
		productName.next().css({"color": "#666666"});
		productName.next().html("* 请输入商品名称");
	}).on("blur",function(){
		if(productName.val() != null && productName.val() != ""){
			/*validateTip(productName.next(),{"color":"green"},imgYes,true);*/
			productName.next().css({"color": "green"});
			productName.next().html("* √");
			pnFlag = true;
		}else{
			/*validateTip(productName.next(),{"color":"red"},imgNo+" 商品名称不能为空，请重新输入",false);*/
			productName.next().css({"color": "orange"});
			productName.next().html("* 商品名称不能为空，请重新输入");
		}
		
	});
	
	productUnit.on("focus",function(){
		/*validateTip(productUnit.next(),{"color":"#666666"},"* 请输入商品单位",false);*/
		productUnit.next().css({"color": "#666666"});
		productUnit.next().html("* 请输入商品单位");
	}).on("blur",function(){
		if(productUnit.val() != null && productUnit.val() != ""){
			/*validateTip(productUnit.next(),{"color":"green"},imgYes,true);*/
			productUnit.next().css({"color": "green"});
			productUnit.next().html("* √");
			puFlag = true;
		}else{
			/*validateTip(productUnit.next(),{"color":"red"},imgNo+" 单位不能为空，请重新输入",false);*/
			productUnit.next().css({"color": "orange"});
			productUnit.next().html("* 商品单位描述不能为空，请重新输入");
		}
		
	});
	
	productCount.on("focus",function(){
		/*validateTip(productCount.next(),{"color":"#666666"},"* 请输入大于0的正自然数，小数点后保留2位",false);*/
		productCount.next().css({"color": "#666666"});
		productCount.next().html("* 请输入大于0的正自然数，小数点后保留2位");
	}).on("keyup",function(){
		this.value = priceReg(this.value);
	}).on("blur",function(){
		this.value = priceReg(this.value);
	});
	
	totalPrice.on("focus",function(){
		/*validateTip(totalPrice.next(),{"color":"#666666"},"* 请输入大于0的正自然数，小数点后保留2位",false);*/
		totalPrice.next().css({"color": "#666666"});
		totalPrice.next().html("* 请输入大于0的正自然数，小数点后保留2位");
	}).on("keyup",function(){
		this.value = priceReg(this.value);
	}).on("blur",function(){
		this.value = priceReg(this.value);
	});

	userName.on("focus",function(){
		/*validateTip(totalPrice.next(),{"color":"#666666"},"* 请输入大于0的正自然数，小数点后保留2位",false);*/
		userName.next().css({"color": "#666666"});
		userName.next().html("* 请输入正确的用户名");
	}).on("keyup",function(){
		this.value = priceReg(this.value);
	}).on("blur",function(){
		this.value = priceReg(this.value);
	});
	
	addBtn.on("click",function(){
		productName.blur();
		productUnit.blur();
		providerId.blur();
		if(pnFlag && puFlag){
			if(confirm("是否确认提交数据")){
				swal('修改成功！','','success').then(function () {
					$("#billForm").submit();
				});
			}
		}else {
			swal("请填写正确信息！","","info");
		}
	});

});