var roleCode = null;
var rcFlag = false;
var roleName = null;
var rnFlag = false;
var addBtn = null;


$(function(){
    roleCode = $("#roleCode");
    roleName = $("#roleName");
    addBtn = $("#add");
    //初始化的时候，要把所有的提示信息变为：* 以提示必填项，更灵活，不要写在页面上
    roleCode.next().html("*");
    roleName.next().html("*");

    /*
     * 验证
     * 失焦\获焦
     * jquery的方法传递
     * ajax后台验证--userCode是否已存在
     */
    roleCode.bind("blur",function(){
        //ajax后台验证--userCode是否已存在
        $.ajax({
            type:"POST",//请求类型
            url:"/role/roleAdd",//请求的url
            data:{method:"rcExist",roleCode:roleCode.val()},//请求参数
            dataType:"json",//ajax接口（请求url）返回的数据类型
            success:function(data){//data：返回数据（json对象）
                if(data.roleCode == "exist"){//账号已存在，错误提示
                    /*validateTip(userCode.next(),{"color":"red"},imgNo+ " 该用户账号已存在",false);*/
                    roleCode.next().css({"color": "red"});
                    roleCode.next().html("* 该职位编码已存在");
                }else if (data.roleCode == "null"){
                    roleCode.next().css({"color": "orange"});
                    roleCode.next().html("* 职位编码不能为空");
                }else{//账号可用，正确提示
                    /*validateTip(userCode.next(),{"color":"green"},imgYes+" 该账号可以使用",true);*/
                    roleCode.next().css({"color": "green"});
                    roleCode.next().html("* √");
                    rcFlag = true;
                }
            },
            error:function(data){//当访问时候，404，500 等非200的错误状态码
                /*validateTip(userCode.next(),{"color":"red"},imgNo+" 您访问的页面不存在",false);*/
                roleCode.next().css({"color": "red"});
                roleCode.next().html("* 您输入的职位编码不正确");
            }
        });
    }).bind("focus",function(){
        //显示友情提示
        /*validateTip(userCode.next(),{"color":"#666666"},"* 用户编码是您登录系统的账号",false);*/
        roleCode.next().css({"color": "#666666"});
        roleCode.next().html("* 请输入职位编码");
    }).focus();

    roleName.bind("blur",function(){
        //ajax后台验证--userCode是否已存在
        $.ajax({
            type:"POST",//请求类型
            url:"/role/roleAdd",//请求的url
            data:{method:"rnExist",roleName:roleName.val()},//请求参数
            dataType:"json",//ajax接口（请求url）返回的数据类型
            success:function(data){//data：返回数据（json对象）
                if(data.roleName == "exist"){//账号已存在，错误提示
                    /*validateTip(userCode.next(),{"color":"red"},imgNo+ " 该用户账号已存在",false);*/
                    roleName.next().css({"color": "red"});
                    roleName.next().html("* 该职位名称已存在");
                }else if (data.roleName == "null"){
                    roleName.next().css({"color": "orange"});
                    roleName.next().html("* 职位名称不能为空");
                }else{//账号可用，正确提示
                    /*validateTip(userCode.next(),{"color":"green"},imgYes+" 该账号可以使用",true);*/
                    roleName.next().css({"color": "green"});
                    roleName.next().html("* √");
                    rnFlag = true;
                }
            },
            error:function(data){//当访问时候，404，500 等非200的错误状态码
                /*validateTip(userCode.next(),{"color":"red"},imgNo+" 您访问的页面不存在",false);*/
                roleName.next().css({"color": "red"});
                roleName.next().html("* 您输入的职位名称不正确");
            }
        });
    }).bind("focus",function(){
        //显示友情提示
        /*validateTip(userCode.next(),{"color":"#666666"},"* 用户编码是您登录系统的账号",false);*/
        roleName.next().css({"color": "#666666"});
        roleName.next().html("* 请输入职位名称");
    }).focus();


    addBtn.bind("click",function(){
        if(rcFlag && rnFlag) {
            if(confirm("是否确认提交数据")){
                swal('添加成功！','','success').then(function () {
                    $("#roleForm").submit();
                });
            }
        } else {
            swal("请填写正确信息！","","info");
        }
    });

});